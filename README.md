# admone
A tolerable Zsh prompt

## Installation
Source this script somewhere in your Zshrc. Theoretically works with plugin managers as well, although this is untested

## Configuration
Configuration is done by modifying the variables set at the start of the script. Just get in there and mess around a bit, its not very difficult to understand

## My design requirements
  - No dependency on OhMyZSH
  - Quick to load
  - Git integration
  - Easily extensible
  - Bracketed sections
  - Vi-mode integration

## Name
```
admon.e              V      2 1 PRES ACTIVE  IMP 2 S    
admoneo, admonere, admonui, admonitus  V (2nd) TRANS   [XXXAO]  
admonish, remind, prompt; suggest, advise, raise; persuade, urge; warn, caution
```

## License
Admone is MIT licensed by [Armaan Bhojwani](https://armaanb.net), 2021.
